# VPC module

| Examle                | Use case                                                          | Cost |
|-----------------------|-------------------------------------------------------------------|------|
| multi-az              | Production or Staging environment                                 |      |
| single-az             | Development or Testing environment                                |      |
| only-public-single-az | Special usage, for example as for single node per region (for AH) |      |

## NACL RULES - PUBLIC SUBNETS

All rules affected IPv4 and IPv6 traffik.

| Ports range  | Ingress | Engress | Rule  | Destination |
|--------------|---------|---------|-------|-------------|
| 22           |   X     |    X    | ALLOW | 0.0.0.0/0   |
| 80           |   X     |    X    | ALLOW | 0.0.0.0/0   |
| 443          |   X     |    X    | ALLOW | 0.0.0.0/0   |
| 1194         |   X     |         | ALLOW | 0.0.0.0/0   |
| 1024 - 65535 |   X     |    X    | ALLOW | 0.0.0.0/0   |
| ALL          |   X     |    X    | ALLOW | VPC_CIDR    |
| ALL          |   X     |    X    | DENY  | 0.0.0.0/0   |

## NACL RULES - PRIVATE SUBNETS

All rules affected IPv4 and IPv6 traffik.

| Ports range  | Ingress | Engress | Rule  | Destination |
|--------------|---------|---------|-------|-------------|
| ALL          |   X     |    X    | ALLOW | VPC_CIDR    |
| ALL          |   X     |    X    | DENY  | 0.0.0.0/0   |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| assign\_ipv6\_address\_on\_creation | Assign IPv6 address on subnet, must be disabled to change IPv6 CIDRs. This is the IPv6 equivalent of map\_public\_ip\_on\_launch | `bool` | `true` | no |
| aws\_region | AWS region to use for all resources | `string` | `"eu-central-1"` | no |
| azs | A list of availability zones in the region | `list(string)` | <pre>[<br>  "eu-central-1a",<br>  "eu-central-1b",<br>  "eu-central-1c"<br>]</pre> | no |
| cidr | The CIDR block for the VPC | `string` | `"10.10.0.0/16"` | no |
| create\_database\_subnet\_group | Should be false to not create a secondary subnet group, colliding with RDS module | `bool` | `false` | no |
| create\_database\_subnet\_route\_table | Controls if separate route table for database should be created | `bool` | `false` | no |
| database\_subnet\_ipv6\_prefixes | Assigns IPv6 database subnet id based on the Amazon provided /56 prefix base 10 integer (0-256). Must be of equal length to the corresponding IPv4 subnet list | `list(number)` | <pre>[<br>  6,<br>  7,<br>  8<br>]</pre> | no |
| database\_subnets | A list of database subnets | `list(string)` | <pre>[<br>  "10.10.51.0/24",<br>  "10.10.52.0/24",<br>  "10.10.53.0/24"<br>]</pre> | no |
| default\_network\_acl\_name | Name to be used on the Default Network ACL | `string` | `"lukapo_default"` | no |
| default\_open\_ports | Default open ports in nacl table for IPv4 and IPv6 | `list(number)` | <pre>[<br>  80,<br>  443,<br>  22<br>]</pre> | no |
| enable\_dhcp\_options | Should be true if you want to specify a DHCP options set with a custom domain name, DNS servers, NTP servers, netbios servers, and/or netbios server type | `bool` | `false` | no |
| enable\_dns\_hostnames | Should be true to enable DNS hostnames in the VPC | `bool` | `false` | no |
| enable\_dns\_support | Should be true to enable DNS support in the VPC | `bool` | `true` | no |
| enable\_dynamodb\_endpoint | Should be true if you want to provision a DynamoDB endpoint to the VPC | `bool` | `true` | no |
| enable\_ipv6 | Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block. | `bool` | `true` | no |
| enable\_nat\_gateway | Should be true if you want to provision NAT Gateways for each of your private networks | `bool` | `true` | no |
| enable\_s3\_endpoint | Should be true if you want to provision an S3 endpoint to the VPC | `bool` | `true` | no |
| manage\_default\_network\_acl | Should be true to adopt and manage Default Network ACL | `bool` | `false` | no |
| one\_nat\_gateway\_per\_az | Should be true if you want only one NAT Gateway per availability zone. Requires `var.azs` to be set, and the number of `public_subnets` created to be greater than or equal to the number of availability zones specified in `var.azs`. | `bool` | `true` | no |
| private\_subnet\_assign\_ipv6\_address\_on\_creation | Assign IPv6 address on private subnet, must be disabled to change IPv6 CIDRs. This is the IPv6 equivalent of map\_public\_ip\_on\_launch | `bool` | `false` | no |
| private\_subnet\_ipv6\_prefixes | Assigns IPv6 private subnet id based on the Amazon provided /56 prefix base 10 integer (0-256). Must be of equal length to the corresponding IPv4 subnet list | `list(number)` | <pre>[<br>  3,<br>  4,<br>  5<br>]</pre> | no |
| private\_subnets | A list of private subnets inside the VPC | `list(string)` | <pre>[<br>  "10.10.1.0/24",<br>  "10.10.2.0/24",<br>  "10.10.3.0/24"<br>]</pre> | no |
| public\_subnet\_ipv6\_prefixes | Assigns IPv6 public subnet id based on the Amazon provided /56 prefix base 10 integer (0-256). Must be of equal length to the corresponding IPv4 subnet list | `list(number)` | <pre>[<br>  0,<br>  1,<br>  2<br>]</pre> | no |
| public\_subnets | A list of public subnets inside the VPC | `list(string)` | <pre>[<br>  "10.10.101.0/24",<br>  "10.10.102.0/24",<br>  "10.10.103.0/24"<br>]</pre> | no |
| reuse\_nat\_ips | Should be true if you don't want EIPs to be created for your NAT Gateways and will instead pass them in via the 'external\_nat\_ip\_ids' variable | `bool` | `true` | no |
| single\_nat\_gateway | Should be true if you want to provision a single shared NAT Gateway across all of your private networks | `bool` | `false` | no |
| tags | n/a | `map(string)` | `{}` | no |
| vpc\_endpoint\_tags | Additional tags for the VPC Endpoints | `map(string)` | `{}` | no |
| vpc\_name | Name to be used on all the resources as identifier | `string` | `"lukapo_default"` | no |

## Outputs

| Name | Description |
|------|-------------|
| database\_subnets | List of IDs of database subnets |
| database\_subnets\_ivp6 | List of IPv6 cidr\_blocks of database subnets in an IPv6 enabled VPC |
| ipv6\_association\_id | The IPv6 CIDR block |
| ipv6\_cidr\_block | The association ID for the IPv6 CIDR block |
| private\_subnets | List of IDs of private subnets |
| private\_subnets\_ipv6 | List of IPv6 cidr\_blocks of private subnets in an IPv6 enabled VPC |
| public\_subnets | List of IDs of public subnets |
| public\_subnets\_ipv6 | List of IPv6 cidr\_blocks of public subnets in an IPv6 enabled VPC |
| vpc\_id | The ID of the VPC |
