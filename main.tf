resource "aws_eip" "nat" {
  count = length(var.public_subnets)
  vpc   = true
  tags = merge(
    {
      "Name" = format("%s", "${var.vpc_name}-${var.azs[count.index]}")
    },
    var.tags
  )
}

module "vpc" {
  source                                         = "terraform-aws-modules/vpc/aws"
  version                                        = "~> 2.0"
  name                                           = var.vpc_name
  cidr                                           = var.cidr
  azs                                            = var.azs
  private_subnets                                = var.private_subnets
  public_subnets                                 = var.public_subnets
  database_subnets                               = var.create_database_subnet_group == true ? var.database_subnets : []
  create_database_subnet_group                   = var.create_database_subnet_group
  enable_nat_gateway                             = var.enable_nat_gateway
  single_nat_gateway                             = var.single_nat_gateway
  one_nat_gateway_per_az                         = var.one_nat_gateway_per_az
  reuse_nat_ips                                  = var.reuse_nat_ips
  external_nat_ip_ids                            = var.reuse_nat_ips == true ? aws_eip.nat.*.id : []
  enable_dns_support                             = var.enable_dns_support
  enable_dns_hostnames                           = var.enable_dns_hostnames
  enable_dhcp_options                            = var.enable_dhcp_options
  tags                                           = var.tags
  enable_ipv6                                    = var.enable_ipv6
  assign_ipv6_address_on_creation                = var.assign_ipv6_address_on_creation
  private_subnet_assign_ipv6_address_on_creation = var.private_subnet_assign_ipv6_address_on_creation
  public_subnet_ipv6_prefixes                    = var.enable_ipv6 == true ? var.public_subnet_ipv6_prefixes : []
  private_subnet_ipv6_prefixes                   = var.enable_ipv6 == true ? var.private_subnet_ipv6_prefixes : []
  database_subnet_ipv6_prefixes                  = var.enable_ipv6 == true && var.create_database_subnet_group == true ? var.database_subnet_ipv6_prefixes : []
  enable_dynamodb_endpoint                       = var.enable_dynamodb_endpoint
  enable_s3_endpoint                             = var.enable_s3_endpoint
  manage_default_network_acl                     = var.manage_default_network_acl
  default_network_acl_name                       = var.vpc_name
  vpc_endpoint_tags                              = { "Name" = format("%s", var.vpc_name) }

}

resource "aws_default_network_acl" "this" {
  default_network_acl_id = module.vpc.default_network_acl_id
  subnet_ids             = module.vpc.public_subnets

  egress {
    protocol   = "all"
    rule_no    = 10
    action     = "allow"
    cidr_block = module.vpc.vpc_cidr_block
    from_port  = 0
    to_port    = 0

  }

  dynamic "egress" {
    for_each = var.enable_ipv6 == true ? var.default_open_ports : []
    content {
      protocol        = "all"
      rule_no         = 20
      action          = "allow"
      ipv6_cidr_block = module.vpc.vpc_ipv6_cidr_block
      from_port       = 0
      to_port         = 0

    }
  }

  dynamic "egress" {
    for_each = var.default_open_ports
    iterator = port
    content {
      protocol   = "tcp"
      rule_no    = port.key + 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = port.value
      to_port    = port.value

    }
  }

  dynamic "egress" {
    for_each = var.enable_ipv6 == true ? var.default_open_ports : []
    iterator = port
    content {
      protocol        = "tcp"
      rule_no         = port.key + 200
      action          = "allow"
      ipv6_cidr_block = "::/0"
      from_port       = port.value
      to_port         = port.value

    }
  }
  dynamic "egress" {
    for_each = var.enable_ipv6 == true ? [
    "1"] : []
    content {
      protocol        = "tcp"
      rule_no         = 2100
      action          = "allow"
      ipv6_cidr_block = "::/0"
      from_port       = 1024
      to_port         = 65535
    }
  }

  egress {
    protocol   = "tcp"
    rule_no    = 1100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535

  }

  dynamic "ingress" {
    for_each = var.enable_ipv6 == true ? [
    "1"] : []
    content {
      protocol        = "tcp"
      rule_no         = 2100
      action          = "allow"
      ipv6_cidr_block = "::/0"
      from_port       = 1024
      to_port         = 65535
    }
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 1100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535

  }

  ingress {
    protocol   = "udp"
    rule_no    = 1000
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1194
    to_port    = 1194

  }

  dynamic "ingress" {
    for_each = var.enable_ipv6 == true ? [
    "1"] : []
    content {
      protocol        = "udp"
      rule_no         = 2000
      action          = "allow"
      ipv6_cidr_block = "::/0"
      from_port       = 1194
      to_port         = 1194
    }
  }

  dynamic "ingress" {
    for_each = var.default_open_ports
    iterator = port
    content {
      protocol   = "tcp"
      rule_no    = port.key + 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = port.value
      to_port    = port.value

    }
  }

  dynamic "ingress" {
    for_each = var.enable_ipv6 == true ? var.default_open_ports : []
    iterator = port
    content {
      protocol        = "tcp"
      rule_no         = port.key + 200
      action          = "allow"
      ipv6_cidr_block = "::/0"
      from_port       = port.value
      to_port         = port.value

    }
  }

  dynamic "ingress" {
    for_each = var.enable_ipv6 == true ? var.default_open_ports : []
    content {
      protocol        = "all"
      rule_no         = 20
      action          = "allow"
      ipv6_cidr_block = module.vpc.vpc_ipv6_cidr_block
      from_port       = 0
      to_port         = 0

    }
  }

  ingress {
    protocol   = "all"
    rule_no    = 10
    action     = "allow"
    cidr_block = module.vpc.vpc_cidr_block
    from_port  = 0
    to_port    = 0

  }

  tags = merge(
    {
      Name = "nacl-${module.vpc.name}-public"
    },
    var.tags
  )
}

resource "aws_network_acl" "main" {
  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  egress {
    protocol   = "all"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0

  }

  dynamic "egress" {
    for_each = var.enable_ipv6 == true ? var.default_open_ports : []
    content {
      protocol        = "all"
      rule_no         = 200
      action          = "allow"
      ipv6_cidr_block = "::/0"
      from_port       = 0
      to_port         = 0

    }
  }

  dynamic "ingress" {
    for_each = var.enable_ipv6 == true ? var.default_open_ports : []
    content {
      protocol        = "all"
      rule_no         = 200
      action          = "allow"
      ipv6_cidr_block = "::/0"
      from_port       = 0
      to_port         = 0

    }
  }

  ingress {
    protocol   = "all"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0

  }

  tags = merge(
    {
      Name = "nacl-${module.vpc.name}-private"
    },
    var.tags
  )
}
