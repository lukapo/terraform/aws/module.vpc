provider "aws" {
  region = "eu-central-1"
}

locals {
  aws_region      = "eu-central-1"
  vpc_cidr        = "10.30.0.0/16"
  vpc_cidr_prefix = "10.30"
  project         = "only-public-single-az"
  environment     = "example"

}

module "vpc" {
  source                   = "../../"
  aws_region               = local.aws_region
  vpc_name                 = "vpc-${local.project}"
  cidr                     = local.vpc_cidr
  azs                      = ["${local.aws_region}a"]
  public_subnets           = ["${local.vpc_cidr_prefix}.0.0/24"]
  private_subnets          = []
  enable_dynamodb_endpoint = false
  enable_s3_endpoint       = false
  enable_nat_gateway       = false
  tags = {
    Project     = local.project
    Environment = local.environment
  }
}

output "public_subnets" {
  value = module.vpc.public_subnets
}

output "public_subnets_ipv6" {
  value = module.vpc.public_subnets_ipv6
}

output "ipv6_association_id" {
  value = module.vpc.ipv6_association_id
}

output "ipv6_cidr_block" {
  value = module.vpc.ipv6_cidr_block
}
