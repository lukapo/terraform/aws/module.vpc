provider "aws" {
  region = "eu-central-1"
}

locals {
  aws_region      = "eu-central-1"
  vpc_cidr        = "10.10.0.0/16"
  vpc_cidr_prefix = "10.10"
  project         = "multi-az"
  environment     = "example"

}

module "vpc" {
  source          = "../../"
  aws_region      = local.aws_region
  vpc_name        = "vpc-${local.project}"
  cidr            = local.vpc_cidr
  azs             = ["${local.aws_region}a", "${local.aws_region}b", "${local.aws_region}c"]
  private_subnets = ["${local.vpc_cidr_prefix}.5.0/24", "${local.vpc_cidr_prefix}.15.0/24", "${local.vpc_cidr_prefix}.25.0/24"]
  public_subnets  = ["${local.vpc_cidr_prefix}.0.0/24", "${local.vpc_cidr_prefix}.10.0/24", "${local.vpc_cidr_prefix}.20.0/24"]
  tags = {
    Project     = local.project
    Environment = local.environment
  }
}

output "private_subnets" {
  value = module.vpc.private_subnets
}

output "public_subnets" {
  value = module.vpc.public_subnets
}

output "private_subnets_ipv6" {
  value = module.vpc.private_subnets_ipv6
}

output "public_subnets_ipv6" {
  value = module.vpc.public_subnets_ipv6
}

output "ipv6_association_id" {
  value = module.vpc.ipv6_association_id
}

output "ipv6_cidr_block" {
  value = module.vpc.ipv6_cidr_block
}
